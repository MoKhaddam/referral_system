<?php

namespace App\Http\Controllers;

use App\Models\Referral;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ReferralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $referrals = Referral::paginate($request->perPage ?? 15);
        return $this->response('success', $referrals);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'type' => 'required|string',
                'description' => 'required|string',
                'datetime' => 'required|date_format:Y-m-d H:i|after_or_equal:' . Carbon::now()->toDateTimeString(),
                'patient_id' => 'required|exists:patients,id',
                'doctor_id' => 'required|exists:doctors,id',
            ]
        );
        if($validator->fails())
            return $this->response('validation error', $validator->errors()->first());
        $request->status = "PENDING";
        $referral = Referral::create($request->toArray());
        $referral->statusLogs()->create(["status" => "PENDING"]);
        return $this->response('success', $referral);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $referral = Referral::findOrFail($id);
        return $this->response('success', $referral);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'status' => 'required|string',
            ]
        );
        if($validator->fails())
            return $this->response('validation error', $validator->errors()->first());
        Referral::where('id', $id)->update(['status' => $request->status]);
        Referral::find($id)->statusLogs()->create(["status" => $request->status]);
        return $this->response('success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
