<?php

namespace App\Http\Controllers;

use App\Services\SecurityService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    private $security;
    public function __construct(SecurityService $security)
    {
        $this->security = $security;
    }

    public function login(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'username' => 'required|string',
                'password' => 'required|string',
                'provider' => 'sometimes|string|in:users,doctors,patients',
            ]
        );
        if($validator->fails())
            return $this->response('validation error', $validator->errors()->first);
        try{
        $user = $this->security->attempt(
            $request['username'],
            $request['password'],
            $request['provider'],
        );
        $user->accessToken = $this->security->createToken($user, $request['provider']);
        return $this->response('success', $user);
        }catch(\Exception $e){
            return $e;
            return $this->response('Invalid username or password');
        }
    }
}
