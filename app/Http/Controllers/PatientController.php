<?php

namespace App\Http\Controllers;

use App\Models\Patient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $patients = Patient::paginate($request->perPage ?? 15);
        return $this->response('success', $patients);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|string',
                'email' => 'required|email',
                'password' => 'required|min:6',
                'phone_number' => 'required|numeric|digits:10',
            ]
        );
        if($validator->fails())
            return $this->response('validation error', $validator->errors()->first());
        $request->password = Hash::make($request->password);
        $patient = Patient::create($request->toArray());
        return $this->response('success', $patient);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $patient = Patient::findOrFail($id);
        return $this->response('success', $patient);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|string',
                'email' => 'required|email',
                'password' => 'required|min:6',
                'phone_number' => 'required|numeric|digits:10',
            ]
        );
        if($validator->fails())
            return $this->response('validation error', $validator->errors()->first());
        $request->password = Hash::make($request->password);
        Patient::where('id', $id)->update($request->toArray());
        return $this->response('success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Patient::where('id', $id)->delete();
        return $this->response('success');
    }
}
