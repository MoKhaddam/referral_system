<?php

namespace App\Http\Controllers;

use App\Models\Doctor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $doctors = Doctor::paginate($request->perPage ?? 15);
        return $this->response('success', $doctors);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|string',
                'email' => 'required|email',
                'password' => 'required|min:6',
                'phone_number' => 'required|numeric|digits:10',
                'type' => 'required|string|in:FULL_TIME,PART_TIME,CONSULTANT',
            ]
        );
        if($validator->fails())
            return $this->response('validation error', $validator->errors()->first());
        $request->password = Hash::make($request->password);
        $doctor = Doctor::create($request->toArray());
        return $this->response('success', $doctor);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $doctor = Doctor::findOrFail($id);
        return $this->response('success', $doctor);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'sometimes|string',
                'email' => 'sometimes|email',
                'password' => 'sometimes|min:6',
                'phone_number' => 'sometimes|numeric|digits:10',
                'type' => 'sometimes|string|in:FULL_TIME,PART_TIME,CONSULTANT',
            ]
        );
        if($validator->fails())
            return $this->response('validation error', $validator->errors()->first());
        if(isset($request['password']))
            $request->password = Hash::make($request->password);
        Doctor::where('id', $id)->update($request->toArray());
        return $this->response('success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Doctor::where('id', $id)->delete();
        return $this->response('success');
    }
}
