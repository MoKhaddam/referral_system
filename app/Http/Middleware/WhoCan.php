<?php

namespace App\Http\Middleware;

use App\Models\Doctor;
use App\Models\Patient;
use App\Models\User;
use Closure;
use \Response;
use Illuminate\Http\Request;

class WhoCan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, bool $users, bool $doctors, bool $patients)
    {
        \Log::info("users: $users, Patients: $patients, doctors: $doctors :");
        if(auth()->user() instanceof User && $users) {
            return $next($request);
        }

        if(auth()->user() instanceof Doctor && $doctors) {
            return $next($request);
        }

        if(auth()->user() instanceof Patient && $patients) {
            return $next($request);
        }

        return Response::json([
            'message' => 'Unauthenticated'
        ], 401);
    }
}
