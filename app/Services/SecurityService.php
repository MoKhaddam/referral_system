<?php

namespace App\Services;

use App\Exceptions\WrongStateException;
use App\Models\Doctor;
use App\Models\Patient;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Hash;

class SecurityService
{

    public function attempt($username, $password, $provider)
    {
        if ($provider === 'doctors') {
            $user = Doctor::where('email', 'LIKE', $username)->first();
        } else if ($provider === 'patients') {
            $user = Patient::where('email', 'LIKE', $username)->first();
        } else {
            $user = User::withTrashed()->where('email', 'LIKE', $username)->first();
        }

        if (!$user || !Hash::check($password, $user->password)) {
            throw new AuthenticationException('Invalid username or password');
        }
        return $user;
    }

    public function createToken($user, $provider, $expiry = null)
    {
        // $token = $user->createToken();
        $token = $user->createToken($provider);
        if ($user instanceof User) {
            $token->token->expires_at = $expiry ?: Carbon::now()->addHours(8)->toDateTimeString();
            $token->token->save();
        }
        return $token->accessToken;
    }
}
