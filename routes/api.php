<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::middleware(['auth:api'])->group(function () {
    Route::middleware(['who_can:1,0,0'])->group( function() {
        Route::resource('doctor', 'App\Http\Controllers\DoctorController');
        Route::resource('user', 'App\Http\Controllers\UserController');
        Route::resource('patient', 'App\Http\Controllers\PatientController')->only(['index', 'store', 'destroy']);
    });
    Route::middleware(['who_can:1,0,1'])->group( function() {
        Route::resource('patient', 'App\Http\Controllers\PatientController')->only(['show', 'update']);
    });
    Route::resource('referral', 'App\Http\Controllers\ReferralController');
});

Route::post('login', 'App\Http\Controllers\LoginController@login')->name('login');
